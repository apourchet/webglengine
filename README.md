WebGLEngine
===========

A simple WebGL engine good for rendering models in an HTML5 canvas.

View Samples:

http://uninote.netii.net/WebGLEngine/examples/colored_triangle/index.html
http://uninote.netii.net/WebGLEngine/examples/textured_square/index.html
http://uninote.netii.net/WebGLEngine/examples/directional_light/index.html
http://uninote.netii.net/WebGLEngine/examples/point_light/index.html
http://uninote.netii.net/WebGLEngine/examples/pointcloud/index.html
http://uninote.netii.net/WebGLEngine/examples/lit_cube/index.html
